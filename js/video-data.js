(function() {
	var app = angular.module('videoData', []);
	app.controller('videoCtrl', function($scope, $sce) {
		$scope.trustUrl = function (url){
			var trustedUrl = $sce.trustAsResourceUrl(url);
			return trustedUrl;
		}
		$scope.array = [
			{src: 'https://www.youtube.com/embed/piY_QXk6ZCI', title: 'TeGro Destiny Montage'},
			{src: 'https://www.youtube.com/embed/uUpx26fxth8', title: 'TeGro Destiny Montage'},
			{src: 'https://www.youtube.com/embed/OfBC_zrJ_9Q', title: 'TeGro' + 's Bench PR'},
			{src: 'https://www.youtube.com/embed/ImTLEyIeges', title: 'TeGro' + 's Deadlift PR'}
		];
	});
})()